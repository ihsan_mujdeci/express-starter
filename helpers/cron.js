/**
 * Created by Ihsan on 4/1/2017.
 */
var CronJob = require('cron').CronJob,
    Users = require('../models/users').Users;

function time({second, minute, hour, dayOfMonth, year, dayOfWeek}){
    return `${second} ${minute} ${hour} ${dayOfMonth} ${year} ${dayOfWeek}`
}

const fourTimesADay ={
    second: '00',
    minute: '15',
    hour: '*/4',
    dayOfMonth: '*',
    year: '*',
    dayOfWeek: '*'
};

var bulkClearAuth = new CronJob({
    cronTime: time(fourTimesADay),
    onTick: Users.clearAuth,
    start: false, /* Start the job right now */
    timeZone: 'Australia/Sydney' /* Time zone of this job.*/
});

