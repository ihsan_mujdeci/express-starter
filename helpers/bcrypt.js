/**
 * Created by Ihsan on 1/21/2017.
 */
var bcrypt = require('bcryptjs'),
    Promise = require('bluebird')

function hash(plainTextPassword){
    return new Promise((resolve, reject)=>{
        bcrypt.genSalt(10, function(err, salt) {
            if(err){
                reject(err)
                return
            }
            bcrypt.hash(plainTextPassword, salt, function(err, hash) {
                if(err){
                    reject(err)
                    return
                }
                resolve(hash)
            });
        });
    })
}

function compare(submittedPassword,dbPassword){
    return new Promise((resolve, reject)=>{
        bcrypt.compare(submittedPassword, dbPassword, function(err, res) {
            if(err){
                reject(err)
                return
            }

            else if(res === false){
                reject({message:'Login credentials incorrect'})
            }
            resolve(res)
        });
    })
}

//hash('password').then(res=>{console.log(res)})
//compare('password', "$2a$10$1VfmCVP5MsgELcRT7T94j.uFpbUlAqwicxh.ADTXYdcjV6Ik83Voy").then(match=>{console.log('match', match)})

module.exports={
    'hash':hash,
    'compare' : compare
}