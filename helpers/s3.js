/**
 * Created by Ihsan on 4/30/2017.
 */
var Promise = require('bluebird'),
    AWS =require('aws-sdk'),
    uuid = require('uuid/v4');

const bucket = "";
const s3Url = "https://s3-ap-southeast-2.amazonaws.com/"+bucket+"/";
//AWS.config.loadFromPath(__dirname+'/../config/aws.json');
//var s3 = new AWS.S3({params:{Bucket: bucket}});

function isUrl(string){
    return (string.includes('https://',0));
}

function getImageData(base64){
    var Body = Buffer.from(dataFromBase64(base64),'base64');
    var ContentType = mimeTypeFromBase64(base64);
    return {Body, ContentType}
}

function mimeTypeFromBase64(base64){
    //eg  "data:image/jpeg;base64,/9j/4AQSkZJRgAB....
    var head = base64.split(';')[0]
    return head.split(':')[1]
}

function dataFromBase64(base64){
    //eg  "data:image/jpeg;base64,/9j/4AQSkZJRgAB....
    return base64.split(',')[1]
}

//Key set to random string if none present
const put = function({Body, ContentType}, Key = uuid()){
    return new Promise((resolve, reject)=>{
        s3.putObject({Key, Body, ContentType, ACL: 'public-read'}, function(err,data){
            if(err){
                reject(err)
            }
            else{
                resolve(s3Url + Key)
            }
        })
    })
}

// optional key input
const putImage = Promise.method(function(base64, key){
    return put(getImageData(base64), key)
        .then(link=>{
            return link
        })
        .catch(err=>{
            return Promise.reject(err)
        })
});

const putImages = Promise.method(function(base64Arr){
    return Promise.map(base64Arr, base64=>{
            if(isUrl(base64))
                return base64;
            return putImage(base64)
        })
        .catch(err=>{
            return Promise.reject({message: 'Something went wrong'})
        })
});

module.exports={
    putImage: putImage,
    putImages : putImages
};