/**
 * Created by ihsanmujdeci on 24/01/2017.
 */
function check(errors){

    if (errors.name === "ValidationError"){
        var arrErrors = []
        for(var e in errors.errors){
            arrErrors.push(errors.errors[e])
        }
        return arrErrors
    }
    if(errors instanceof Error){
        return [{message: errors.message}]
    }
    else if(!Array.isArray(errors)){
        return [errors]
    }
    return errors
}

module.exports=check