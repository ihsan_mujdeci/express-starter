/**
 * Created by Ihsan on 5/5/2017.
 */
function getUrl(request){
    return request.protocol + '://' + request.get('host')
}

function isWindows(){
    return (process.platform === 'win32')
}

function flatten(p,c){
    return p.concat(c)
}

function notEmptyArray(arr){
    return (Array.isArray(arr) && arr.length > 0)
}

function deepCopy(obj){
    return JSON.parse(JSON.stringify(obj))
}

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


module.exports= {
    getUrl,
    isWindows,
    flatten,
    emailRegex,
    notEmptyArray,
    deepCopy
}