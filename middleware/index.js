/**
 * Created by Ihsan on 4/30/2017.
 */
/**
 * Created by Ihsan on 1/22/2017.
 */
var Users = require('../models/users').Users,
    moment = require('moment');

function checkAuth(req, res, next){
    var xAuth = req.headers['x-auth-token']

    Users.validAuth(xAuth)
        .then(found=>{
            req.userId = found._id;
            req['user'] = found;
            found.refreshAuth(xAuth);
            next();
        })
        .catch(()=>{
            res.status(401).send({message: "Invalid Auth"});
        });
}

function checkAuthAdmin(req, res, next){
    var xAuth = req.headers['x-auth-token']

    Users.validAuth(xAuth, ['admin'])
        .then(found=>{
            req.userId = found._id;
            req['user'] = found;
            found.refreshAuth(xAuth);
            next();
        })
        .catch(()=>{
            res.status(401).send({message: "Invalid Auth"});
        });
}

//TESTING NEEDED
function userInfo(req, res, next){
    var xAuth = req.headers['x-auth-token']

    Users.validAuth(xAuth)
        .then(found=>{
            req.userId = found._id;
            req['user'] = found;
            found.refreshAuth(xAuth);
            next();
        })
        .catch(()=>{

        });
    next();
}

module.exports={
    'checkAuth': checkAuth,
    checkAuthAdmin : checkAuthAdmin,
    userInfo: userInfo
}
