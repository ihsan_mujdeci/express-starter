/**
 * Created by Ihsan on 4/30/2017.
 */
/**
 * Created by Ihsan on 1/21/2017.
 */

const fs = require("fs")
const config = getConfig()

var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('./models/mongoose'),
    expressValidator = require('express-validator'),
    cron = require('./helpers/cron'),
    moment = require('moment-timezone'),
    tools = require('./helpers/tools'),
    usersRouter = require('./routes/users').usersRouter

var app = express();

app.use(bodyParser.json({}));//limit:ex '50mb'
app.use(expressValidator({
    errorFormatter: (param, msg, value) => {
        return {param : param, message : msg, value : value};
    },
    customValidators:{
        isArray: value => Array.isArray(value),
        notEmptyArray : value=> (Array.isArray(value) && value.length > 0),
        isEmail:value => tools.emailRegex.test(value)
    }
}));
app.use(bodyParser.urlencoded(
    {extended: true}//,limit: '50mb'
));

app.use('/node_modules', express.static(__dirname + '/node_modules/'));

app.use((req, res, next)=>{
    //console.log(req.originalUrl)
    //console.log(req.body)
res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, X-Auth-Token, search");
res.header("Access-Control-Allow-Origin", "*");
res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS, PATCH');
if ('OPTIONS' == req.method)
    res.send(200);
else
    next();
});

app.use('/users', usersRouter);


app.listen(config.http.port);
console.log("App listening on port " + config.http.port);

//error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    //res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    console.log(err)
    res.status(err.status || 500);
    res.send({error: err})
});

module.exports = app;

function getConfig(){
    try {
        return JSON.parse( fs.readFileSync('server.json', 'utf8') )
    } catch (err) {
        console.log("Please set up config file")
        console.log('Copy "server.json.default" to "server.json" and edit appropriately')
        process.exit()
    }
}
