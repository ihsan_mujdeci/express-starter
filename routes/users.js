/**
 * Created by Ihsan on 4/30/2017.
 */
/**
 * Created by Ihsan on 2/25/2017.
 */
var express = require('express'),
    router = express.Router(),
    s3 = require('../helpers/s3'),
    Users = require('../models/users').Users,
    errorCheck = require('../helpers/errorCheck'),
    authCheck = require('../middleware/index').checkAuth,
    checkAuthAdmin = require('../middleware/index').checkAuthAdmin,
    _ = require('lodash'),
    Promise = require('bluebird'),
    bcrypt = require('../helpers/bcrypt')

//router.use(authCheck);

router.patch('/profilePicture',authCheck, (req,res)=>{
    var image = req.body.image

    var editingId = req.userId;

    req.checkBody('image', 'Image cannot be empty').notEmpty();

    req.getValidationResult()
        .then(result=>{
            if(!result.isEmpty())
                return Promise.reject(result.array());
        })
        .then(()=>{
            return s3.putImage(image)
        })
        .then(s3InsertKey=>{
            return Users.update({_id: editingId}, {image:s3InsertKey}).exec()
        })
        .then(inserted=>{
            if(inserted.n === 0){
                return Promise.reject({message: 'Error with insertion'})
            }
            res.status(200).send({message: 'Profile picture updated'})
        })
        .catch(err=>{
            res.status(400).send(errorCheck(err))
        })
});

router.patch('/:userId', authCheck,(req,res)=>{
    req.checkBody('name.first', 'First name cannot be empty').notEmpty();
    req.checkBody('name.last', 'Last name cannot be empty').notEmpty();
    req.checkBody('email', 'Email cannot be empty').notEmpty();

    const editingId = req.userId;
    req.body= _.pick(req.body, ['name', 'email', 'user', 'roles']);

    Promise.try(()=>{
            return req.getValidationResult()
                .then(result=>{
                    if(!result.isEmpty())
                        return Promise.reject(result.array());

                    return Users.update({_id: editingId},{$set:req.body}, {runValidators: true}).exec()
                })
                .then(found=>{
                    if(found.n === 0){
                        return Promise.reject({message: 'Something went wrong with updates'})
                    }
                    else{
                        return res.status(200).send({message: 'Account updated'})
                    }
                })
        })
        .catch(err=>{
            res.status(400).send(errorCheck(err))
        })

});

router.patch('/', authCheck,(req,res)=>{
    req.checkBody('name.first', 'First name cannot be empty').notEmpty();
    req.checkBody('name.last', 'Last name cannot be empty').notEmpty();
    req.checkBody('email', 'Email cannot be empty').notEmpty();

    const editingId = req.userId;
    req.body= _.pick(req.body, ['name', 'email', 'user'])

    Promise.try(()=>{
            return req.getValidationResult()
                .then(result=>{
                    if(!result.isEmpty())
                        return Promise.reject(result.array());
                })
                .then(()=>{
                    return Users.uniqueEmailExceptThis(editingId, req.body.email)
                })
                .then(()=>{
                    return Users.update({_id: editingId},{$set:req.body}, {runValidators: true}).exec()
                })
                .then(found=>{
                    if(found.n === 0){
                        return Promise.reject({message: 'Something went wrong with updates'})
                    }
                    else{
                        return res.status(200).send({message: 'Account updated'})
                    }
                })
        })
        .catch(err=>{
            res.status(400).send(errorCheck(err))
        })

})

router.get('/', checkAuthAdmin, (req,res)=>{ //working
    Users.find({},'_id email createdAt roles name image').exec()
        .then(found=>{
            res.status(200).send(found)
        })
        .catch(err=>{
            res.status(400).send(errorCheck(err))
        })
});

router.delete('/:userId', checkAuthAdmin, (req,res)=>{ //check working
    const delUserId = req.params.userId

    if(delUserId === req.userId){
        res.status(401).send([{message: "You can't delete yourself"}])
        return;
    }

    Promise.try(()=>{
            return req.getValidationResult()
                .then(result=>{
                    if(!result.isEmpty())
                        return Promise.reject(result.array());
                })
                .then(()=>{
                    console.log(delUserId);
                    return Users.find({_id: delUserId}).remove().exec()
                })
                .then(removed=>{
                    if(removed.result.n ===0){
                        return Promise.reject({message: 'Something went wrong with deletion'})
                    }
                    res.status(200).send({message: 'User deleted'})
                })
        })
        .catch(err=>{
            console.log(err);
            res.status(400).send(errorCheck(err))
        })
});

router.put('/', checkAuthAdmin,(req, res)=>{
    var user = _.pick(req.body,["email","name","password","roles"]);
    var newUser = new Users(user)
    var error = newUser.validateSync();

    if(error && error.name === 'ValidationError'){
        res.status(400).send(error)
        return
    }

    bcrypt.hash(user.password)
        .then(hashed=>{
            user.password = hashed;
            newUser = new Users(user);
            return Users.uniqueEmail(user.email);
        })
        .then(()=>{
            return newUser.save();
        })
        .then(inserted=>{
            res.status(201).send({message: 'User inserted'});
        })
        .catch(error=>{
            res.status(400).send(errorCheck(error));
        })
});

router.get('/me', authCheck,(req,res)=>{//done
    const getUserId = req.userId;

    Users.findOne({_id: getUserId}, '_id email createdAt roles name company').exec()
        .then(user=>{
            if(!user){
                return Promise.reject({message:'User doesn\'t exist'})
            }
            res.status(200).send(user)
        })
        .catch(err=>{
            res.status(400).send(user)
        })
});

router.get('/:userId', checkAuthAdmin,(req,res)=>{//working
    const getUserId = req.params.userId

    Users.findOne({_id: getUserId}, '_id email createdAt roles name company').exec()
        .then(user=>{
            if(!user){
                return Promise.reject({message:'User doesn\'t exist'})
            }
            res.status(200).send(user)
        })
        .catch(err=>{
            res.status(400).send(errorCheck(err))
        })
});

router.post('/login', (req,res)=>{
    var email = req.body.email,
        password = req.body.password;

    var user;
    Users.login(email, password)
        .then(loggedIn=>{
            user = loggedIn;
            return loggedIn.createSession(user._id)
        })
        .then(sessionId=>{
            var data={
                message: "Successfully logged in",
                data:{
                    'X-Auth-Token': sessionId,
                    account:{
                        _id: user._id,
                        name : user.name,
                        email: user.email
                    }
                }
            }
            res.status(200).send(data)
        })
        .catch(error=>{
            res.status(400).send(error)
        })
})

router.get('/logout', (req,res)=>{
    var xAuth = req.headers['x-auth-token']

    Users.deleteSession(xAuth)
        .then(deleted=>{
            res.status(200).send(deleted)
        })
        .catch(error=>{
            res.status(400).send(error)
        })
});

module.exports ={
    "usersRouter" : router
}
