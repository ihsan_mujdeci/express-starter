/**
 * Created by Ihsan on 4/30/2017.
 */
const fs = require("fs")
const config = JSON.parse( fs.readFileSync('server.json', 'utf8') );

var mongoose = require('mongoose')
mongoose.Promise = require('bluebird');

//var auth = {
//    user: config.database.username,
//    pass: config.database.password
//}

var db = mongoose.createConnection(config.database.url);//, auth

db.on('error', console.error.bind(console, 'DB error:'));
db.on('connecting', function () {console.log('DB connecting')});
db.on('connected', function () {console.log('DB connected')});
db.on('open', function () {console.log('DB open')});

module.exports={
    mongoose,
    "DB":db,
    "Schema" : mongoose.Schema
}