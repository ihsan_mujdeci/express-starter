/**
 * Created by Ihsan on 4/30/2017.
 */
/**
 * Created by Ihsan on 1/21/2017.
 */
var mongoose = require("./mongoose"),
    moment = require('moment'),
    bcrypt = require("../helpers/bcrypt.js"),
    UUID = require("uuid/v4"),
    Promise = require('bluebird'),
    tools = require('../helpers/tools'),
    mailer = require('../helpers/mailer');

var emailRegex = tools.emailRegex

var users = new mongoose.Schema({
    email: {
        validate:{
            validator: v=> emailRegex.test(v),
            message: '{VALUE} is not a valid email'
        },
        type: String, required: [true, 'Email cannot be empty']
    },
    name:{
        first: {type: String, required: [true, 'First name cannot be empty']},
        last: {type: String, required: [true, 'Last name cannot be empty']}
    },
    roles: {type: [String] },
    createdAt: {type: Date, default: Date.now},
    resetPassword: {
        _id: String,
        expiration: Date
    },
    auth: [
        {
            token : String,
            expiration : Date
        }
    ],
    image: String
}, {collection: 'users'});


users.statics.findUser = function(userId, fields){
    return this.findOne({_id: userId}, fields).exec()
        .then(found=>{
            if(!found){
                throw {message:'User not found'};
            }
            return found
        })
};

users.statics.login = function(email, plaintextPassword){
    var foundUser

    this.findOne({email}).exec()
        .then(found=>{
            if(!found){
               throw {message:'Login credentials incorrect'}
            }
            foundUser = found
            return bcrypt.compare(plaintextPassword, found.password)
        })
        .then(()=>{
            return foundUser
        })
}

users.statics.uniqueEmail = function (email){
    return this.findOne({email:email}).exec()
        .then(found=>{
            if(found){
                throw {message: 'Duplicate email exists'}
            }
            return true
        })
};

users.methods.createSession = function(_id){

    var token = UUID();

    return Users.update({_id: this._id}, {
        $push:{
            auth: {
                token: token, expiration: moment().add(6,'h')
            }
        }
    }).exec()

};

users.statics.deleteSession = function(token){

    return this.update({}, {$pull:{auth:{token: token}}}).exec()
        .then(removed=>{
            if(removed.nModified === 0){
                throw {message: "Auth token not found"}
            }
            return {message: "Successfully logged out"}
        })
        .catch(error=>{
            reject(error)
            })
}

users.statics.uniqueEmailExceptThis = function(_id, email){
    return Users.findOne({_id: {$ne: _id}, email: email}).exec()
        .then(found=>{
            if(found){
                throw {message: 'This email is already taken'}
            }
            else{
                return true
            }
        })
};

users.statics.validAuth = function(auth, roles){
    if(!auth)
        throw {message: "No auth"};

    let query = {};
    if(roles)
        query.roles = {$in: roles};

    query.auth = {
        $elemMatch:{
            token: auth, expiration : {$gt: moment()}
        }
    };

    return this.findOne(query).exec()
        .then(found => found ? found : Promise.reject({'message':"Auth not valid"}))
};

users.methods.refreshAuth = function(auth){
    const freshHours = 6;
    Users.update({_id: this._id, 'auth.token': auth}, {$set:{'auth.$.expiration': moment().add(freshHours, 'h')}}).exec();
};

users.statics.clearAuth = function(){
    return this.update({}, {$pull: {auth:{expiration:{$lt: moment()}}}}, {multi: true}).exec()
};

users.methods.setPassword = function(password){
    return new Promise((resolve, reject)=>{
        bcrypt.hash(password)
            .then(hashedPassword=>{
                return Users.update({_id: this._id}, {password: hashedPassword}).exec()
            })
            .then(updated=>{
                if(updated.n ===0){
                    reject({message: 'Oops something went wrong when updating your password'})
                }
                else{
                    resolve({message: 'Password successfully set'})
                }
            })
    })
};

users.methods.createResetPasswordToken = function(){
    return new Promise((resolve, reject)=>{

        var token = UUID()
        Users.update({_id: this._id}, {$set:{
            resetPassword:{
                token: token, expiration: moment().add(6,'h')
            }
        }
        }).exec()
            .then(updated=>{
                if(updated.n ===0){
                    reject({message:"Couldn't create reset token"})
                }
                else{
                    resolve(token)
                }
            })
    })
};

users.methods.clearResetPassword = function(){
    return new Promise((resolve, reject)=>{

        Users.update({_id: this._id},{$unset:{'resetPassword.token': true,  'resetPassword.expiration': true}}).exec()
            .then(updated=>{

                if(updated.n === 0){
                    reject({message: 'Oops something went wrong when clearing old password token'})
                }
                resolve(true)
            })
    })
}

users.methods.verificationEmail = Promise.method(function(url){
    var randomStr = UUID();

    var link = url+"/verify-email/"+randomStr

    var mail ={
        to: this.email,
        subject : 'Tenant Reports account verification',
        html : mailer.linkEmail(
            'Verify account',
            `
                Hey from the team at xxxx<br>
                Thanks for signing up and using our app<br>
                Click the link below to verify your account
            `,
            link,
            'Verify email'
        )
    }

    Users.update({_id: this._id}, {$set: {verifyEmailLink: randomStr}}).exec();
    mailer.send(mail)
});

users.statics.initAdmin =  function(){
    Users.find({}).exec()
        .then(found=>{
            if(!found){
                return bcrypt.hash('password');
            }
        })
        .then(hashPassword=>{
            new Users({email: 'admin@admin.com',roles:['admin'], password: hashPassword,name:{first: 'Admin', last: 'User'}}).save()
        });
};

var Users = mongoose.DB.model('Users', users);
Users.initAdmin();

module.exports.Users = Users;



